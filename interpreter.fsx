﻿type variable = string
type Op = 
| Sum 
| Diff 
| Mult 
| Div 
| Equal
| LesserEqual
| GreaterEqual
| Lesser
| Greater
| Different

type expr =
| Num of int
| Bool of bool
| Bop of expr * OP * expr
| If of expr * expr * expr
| Var of variable
| App of expr * expr
| Func of variable * expr
| Let of variable * expr * expr
| Lrec of variable * expr * expr
| Nil of Option<"i">
| AtrList of expr * expr
| IsEmpty of expr
| Hd of expr
| Tl of expr
| Raise of expr
| Try of expr * expr