﻿type OP = 
| Sum 
| Diff 
| Mult 
| Div 
| Equal
| LEqual
| GEqual
| Less
| Greater
| Different

type expr =
| Num of int
| Bool of bool
| Bop of expr * OP * expr
| If of expr * expr * expr
| Var of string
| App of expr * expr
| Func of string * expr
| Let of string * expr * expr
| Lrec of string * expr * expr
| Nil
| ConsList of expr * expr
| IsEmpty of expr
| Hd of expr
| Tl of expr
| Raise
| Try of expr * expr

type value = 
| Vnum
| Vbool
| Vnil
| Vlist of value * value
| Vfun of value

let isInt x = box x :? int

let isValue (e:expr) : bool =
    match e with
    |Num(n) -> true
    |Bool(b) -> true
    |Nil -> true
    |ConsList(e1,e2) -> true
    |Func(var,e1) -> true
    |_ -> false

let rec replace[variavel, e:expr, valor:expr] =
        match e with
        |Var(x) -> if (variavel = x) then valor else Var(x)
        |Bop(e1,op,e2) -> Bop(replace[variavel, e1, valor],op,replace[variavel, e2, valor])
        |If(e1, e2, e3) -> If(replace[variavel, e1, valor], replace[variavel, e2, valor], replace[variavel, e3, valor])
        |App(e1, e2) -> App(replace[variavel, e1, valor], replace[variavel, e2,valor])
        |Func(var,e1) when variavel <> var -> Func(var, replace[variavel,e1,valor])
        |Let(var,e1,e2) when variavel <> var -> Let(var, replace[variavel,e1,valor], replace[variavel,e2,valor])
        |Let(var,e1,e2) -> Let(var, replace[variavel,e1,valor], e2)
        |Lrec(var,e1,e2) when variavel <> var -> Lrec(var, replace[variavel,e1,valor], replace[variavel,e2,valor])
        |ConsList(e1,e2) -> ConsList(replace[variavel,e1,valor], replace[variavel,e2,valor])
        |IsEmpty(e1) -> IsEmpty(replace[variavel,e1,valor])
        |Hd(e1) -> Hd(replace[variavel,e1,valor])
        |Tl(e1) -> Tl(replace[variavel,e1,valor])
        |Try(e1,e2) -> Try(replace[variavel,e1,valor], replace[variavel,e2,valor])
        |_ when isValue(e) -> e
        |_ -> e


//começar avaliador de expressões
let rec step (e: expr) : expr = 
    match e with
    |Num n -> Num n
    |Bool b -> Bool b
    |Func(var, e1) -> Func(var, e1)
    |Nil -> Nil
    

    //Bop
    |Bop(Num(n1),op,Num(n2)) ->
        (match op with
            |Sum -> Num(n1 + n2)
            |Diff -> Num(n1 - n2)
            |Mult -> Num(n1 * n2)
            |Div -> Num(n1 / n2)
            |Equal -> Bool(n1 = n2)
            |LEqual -> Bool(n1 <= n2)
            |GEqual -> Bool(n1 >= n2)
            |Less -> Bool(n1 < n2)
            |Greater -> Bool(n1 > n2)
            |Different -> Bool(n1 <> n2)
          )
    |Bop(Raise,op,e2) -> Raise
    |Bop(e1,op,Raise) when isValue(e1) -> Raise
    |Bop(e1,op,e2) when isValue(e1) -> let e2' = step e2 in step(Bop(e1,op,e2'))
    |Bop(e1,op,e2) when not(isValue(e1)) -> let e1' = step e1 in step(Bop(e1',op,e2))
            
    //If
    |If(Bool(true),e1,e2) when isValue(e1) && isValue(e2) -> e1
    |If(Bool(false),e1,e2) when isValue(e1) && isValue(e2) -> e2
    |If(Raise,e1,e2) -> Raise
    |If(e1,e2,e3) when not(isValue(e1)) -> let e1' = step e1 in step(If(e1', e2, e3))
    |If(e1,e2,e3) when isValue(e1) && not(isValue(e2)) -> let e2' = step e2 in step(If(e1, e2', e3))
    |If(e1,e2,e3) when isValue(e1) && isValue(e2) && not(isValue(e3)) -> let e3' = step e3 in step(If(e1, e2, e3'))

    //App
    //replace(NomeDaVariavel, expressao, valorQueVaiSubstituir)
    //App(Func(variavel,expressao),valor)
    |App(Func(var,e1),e2) when isValue(e2) -> step (replace[var,e1,e2])
    |App(e1,e2) when isValue(e1) -> let e2' = step e2 in step(App(e1,e2'))
    |App(Raise,e2) -> Raise
    |App(e1,e2) -> let e1' = step e1 in step(App(e1',e2))

    //Let
    //replace(NomeDaVariavel, expressao, valorQueVaiSubstituir)
    //Let(variavel, valor, expressao)
    |Let(var,v,e2) when isValue(v) -> step (replace[var,e2,v])
    |Let(var,e1,e2) -> let e1' = step e1 in step(Let(var,e1',e2))
    
    //Lrec
    |Lrec(var1, Func(var2, e1), e2) -> step (replace[var1,(Func(var2,Lrec(var1,Func(var2,e1), e1))), e2])
    //|Lrec(var1, e1, e2) -> let e1' = step e1 in step(Lrec(var1, e1', e2))
    
 
    //ConsList
    |ConsList(e1,e2) when isValue(e1) && isValue(e2) -> ConsList(e1,e2)
    |ConsList(e1,Raise) -> Raise
    |ConsList(e1,e2) when isValue(e1) -> let e2' = step e2 in step(ConsList(e1,e2'))
    |ConsList(Raise,e2) -> Raise
    |ConsList(e1,e2) -> let e1' = step e1 in step(ConsList(e1',e2))

    //IsEmpty
    |IsEmpty(Nil) -> Bool(true)
    |IsEmpty(Raise) -> Raise
    |IsEmpty(ConsList(e1,e2)) -> Bool(false)

    //Hd
    |Hd(Nil) -> Raise
    |Hd(ConsList(e1,e2)) when isValue(e1) && isValue(e2) -> e1
    |Hd(Raise) -> Raise
    |Hd(e1) -> let e1' = step e1 in step(Hd(e1'))

    //Tl
    |Tl(Nil) -> Raise
    |Tl(ConsList(e1,e2)) when isValue(e1) && isValue(e2) -> e2
    |Tl(Raise) -> Raise
    |Tl(e1) -> let e1' = step e1 in step(Tl(e1'))

    //Try
    |Try(e1,e2) when isValue(e1) -> e1
    |Try(Raise,e2) -> step e2
    |Try(e1,e2) -> let e1' = step e1 in step(Try(e1',e2))

    //--
    |_ -> Raise //Nenhuma regra se aplica



let e1 = Bop(Num(3), LEqual, (Bop(Num(3), Sum, Num(2))))
let teste = step e1 //true

let e2 = Bop((Bop(Num(3), Sum, Num(5)), LEqual, (Bop(Num(3), Sum, Num(2)))))
let teste2 = step e2 //false

let e3 = If(e2, e1, Bop(Num(2), Sum, Num(4)))
let teste3 = step e3 //6

let lista = ConsList(Num(5),ConsList(Num(4),ConsList(Num(3),Nil)))

let e4 = IsEmpty(lista)
let teste4 = step e4 //false

let e5 = IsEmpty(Nil)
let teste5 = step e5 //true

let e6 = Tl(lista)
let teste6 = step e6 //ConsList(Num(4),ConsList(Num(3),Nil))

let e7 = Try(Hd(ConsList(Num(4),Nil)),Raise)
let teste7 = step e7 //4

let e8 = App(Func("x", Bop(Var("x"),Sum,Num(5))), Num(5))
let teste8 = step e8 //10

let e9 = Let("x",Num(5),Bop(Num(4),Sum,Var("x")))
let teste9 = step e9 //9

let e10 = App(Func("x", If(Bop(Var("x"), GEqual, Num(0)), Bop(Bop(Var("x"),Sum,Num(1)),Sum,Num(1)), Num(5))), Num(1))
let teste10 = step e10 //3

let e11 = App(Func("x", App(Func("y",Bop(Var("x"),Sum,Var("y"))),Num(2))),Num(1))
let teste11 = step e11 //3

let e12 = App(Func("x", Let("y",Num(5),Bop(Var("y"),Sum,Var("x")))), Num(3))
let teste12 = step e12 //8

let e13 = App(Func("x", Hd(ConsList(Var("x"),ConsList(Num(1),Nil)))), Num(2))
let teste13 = step e13 //2

let e14 = App(Func("x", Tl(ConsList(Num(1),ConsList(Var("x"),Nil)))), Num(2))
let teste14 = step e14 //ConsList(Num(2),Nil)

let e15 = App(Func("x", Try(Var("x"),Bop(Num(1),Sum,Num(2)))), Bop(Num(1),Sum,Num(4)))
let teste15 = step e15 //5

let e16 = Let("x", Num(3), Let("y", Num(6), Bop(Var("y"), Sum, Var("x"))))
let teste16 = step e16 //9

let e17 = App(Func("y", e16), Num(7))
let teste17 = step e17 //9

let e18 = Let("x", Num(3), Bop(Var("y"), Sum, Var("x")))
let teste18 = step e18 //Raise

let e19 = App(Func("y", e18), Num(2))
let teste19 = step e19 //5



let fat = If(Bop(Var("x"), Equal, Num(0)),Num(1),Bop(Var("x"),Mult,App(Var("fat"),Bop(Var("x"),Diff, Num(1)))))
let callfat = App(Var("fat"), Num(5))
let funfat = Func("x", fat)
let e20 = Lrec("fat", funfat, callfat)
let teste20 = step e20
